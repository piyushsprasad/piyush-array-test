CREATE DATABASE IF NOT EXISTS authserver_db;

CREATE user IF NOT EXISTS 'authserver_admin'@'%' identified BY 'complicated_password';
GRANT all ON authserver_db.* TO 'authserver_admin'@'%';

USE authserver_db;

/* Used for Authserver v1. */
CREATE TABLE IF NOT EXISTS authentication (
    user_id BigInt NOT NULL AUTO_INCREMENT,
    email_address varchar(255) NOT NULL,
    hashed_password varchar(255) NOT NULL,
    scopes tinyblob,
    PRIMARY KEY (email_address),
    KEY user_id (user_id)
);

/* Used for Authserver v2. */
CREATE TABLE IF NOT EXISTS account (
    user_id BigInt NOT NULL AUTO_INCREMENT,
    email_address varchar(255) NOT NULL,
    hashed_password varchar(255) NOT NULL,
    PRIMARY KEY (email_address),
    KEY user_id (user_id)
);

CREATE TABLE IF NOT EXISTS session (
    token varchar(255) NOT NULL,
    email_address varchar(255) NOT NULL,
    expiration_time_seconds BigInt NOT NULL,
    PRIMARY KEY (token)
);