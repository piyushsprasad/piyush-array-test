FROM gradle:4.7.0-jdk8-alpine AS build
COPY --chown=gradle:gradle . /authserver/gradle/src
WORKDIR /authserver/gradle/src
RUN gradle build --no-daemon 

FROM adoptopenjdk/openjdk11:jre-11.0.9.1_1-alpine
VOLUME /tmp
ARG JAVA_OPTS
ENV JAVA_OPTS=$JAVA_OPTS
RUN mkdir /app
COPY --from=build /authserver/build/libs/authserver-0.0.1-SNAPSHOT.jar /app/authserver/build/libs/authserver-0.0.1-SNAPSHOT.jar piyusharraytest.jar
EXPOSE 8080
EXPOSE 8443
# # For Spring-Boot project, use the entrypoint below to reduce Tomcat startup time.
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar piyusharraytest.jar --spring.config.location /authserver/src/main/resources/application.properties