# Piyush Backend Test

## High Level Design

In order to fully adhere to RESTful practices, I first implemented a stateless authentication flow, meaning that the server has no concept of session/state and does not track who is currently logged in. When the user logs in, we return a JSON Web Token (JWT) containing all the information the server needs to authorize the request (email address of the token, expiration time, and list of scopes the user is authorized to access). The JWT is encoded, signed and returned as a string. When the user makes further requests, they have to pass in this JWT string in the authorization header. The server decodes the JWT and checks if the user is authorized to access the resource without having to query any databases. The JWT is generally saved by clients in local storage or cookies. To log out, the client simply has to delete the JWT from where they saved it. This is **v1**.

One downsides of stateless auth is that the server cannot log out the user (as it has no concept of session). After talking to Asad, and in order to show how a system with server side log out would work, I also implemented stateful authentication. When the user logs in, we create a session on the backend, save it to the db, and return a token that corresponds to this session. When the user makes further requests, they pass this token in the authorization header. The server compares it to what is saved in the backend to verify if the token is valid and whether the user has access to the resource. To log out, the user sends a DELETE request and we delete the session from our db. This is **v2**.

The sign up flow is same for both, we simply save the email address and hashed password in the Authentication table for v1 or the Account table for v2. When logging in, the inputted email address and password are compared to what is stored in the Authentication/Account table.

**Benefits of Stateless over Stateful**
- Scales much better as the server does not have to query the db to verify the user's state for each request.
- Adheres to statelessness aspect of RESTful design (that the server should not keep client state on server)
- Easier & less work for 3P services to use the credentials as the 3P does not have to query the sessions state on our server.

**Benefits of Stateful over Stateless**
- Better security and control as you can revoke the session at any time.
- Can modify session data at any time. For stateless auth if we wanted to modify the session, we would have to ask the client to do so. This is more complicated and we also cannot verify if they actually modify the session.
- Relatively simpler to implement as it does not require the complexity of the JWT encoding/decoding.

**Which is better?**

I think it generally comes down to scalability/cost vs control. I first implemented the stateless flow as I had thought one of the requirements of the project was adhering to RESTful practices. However, if cost is not an issue, I would probably choose the stateful auth flow due to the greater control the server has over the users' sessions. If server costs do become an issue and scaling is a priority, then the stateless flow is a great solution.

### **HTTPS over HTTP**

As security is an important part of any authentication system, I enabled HTTPS (and forwarded HTTP to HTTPS) for this application. In order to do this, I had to generate self-signed SSL certificate, add it to the keystore, and configure it in the application.properties.

As I used a self-signed certificate, it will not be trusted by any browser (or CURL). That is why all the requests are using the "-k" or insecure flag. In a production environemnt, we would be using a proper certificate issued by a trusted Certificate Authority and would not need to do this.

## Setup

The SQL code to setup the database, user, and tables is in `scripts/create_db_and_tables.sql`.

The table creation part of the script is not fully necessary as Hibernate maps Java objects annotated with `@Entity` to database tables. However, creating the tables through SQL allows us more customization than through the Java code. For example, I wanted the user_id field to autoincrement without being the primary key - doing this with Hibernate would have been very messy.

You do not need to manually run the sql script, as `docker-compose.yml` is configured to start mySQL and run the script before then building and starting Authserver:

```bash
docker-compose up -d
```

## How to Use

With the server running, you can now send requests.

### **v1**

Stateless authentication (v1) uses two resources: `/authentications` and `/auth-tokens`.

#### **Sign Up**

To sign up a new user, you send a POST request to `/authentications` with the email address and password as arguments in the body:

```bash
curl -k -v https://localhost:8443/authentications -d emailAddress=[email_address] -d password=[password]

#Example:
curl -k -v https://localhost:8443/authentications -d emailAddress=abc5@gmail.com -d password=abc123456
```

Returns 200 if success or 409 if the email address is already used. This will create an entry for the user in the Authentication table.

#### **Log In**

To log in a signed-up user, you send a POST request to `/auth-tokens` with the email address and password as arguments in the body:

```bash
curl -k -v https://localhost:8443/auth-tokens -d emailAddress=[email_address] -d password=[password]

#Example:
curl -k -v https://localhost:8443/auth-tokens -d emailAddress=abc5@gmail.com -d password=abc123456
#Returns:
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbF9hZGRyZXNzIjoiYWJjNEBnbWFpbC5jb20iLCJhdXRob3JpemVkX3Njb3BlcyI6WyJHRVRfQVVUSEVOVElDQVRJT04iLCJERUxFVEVfQVVUSEVOVElDQVRJT04iXSwiaXNzIjoiYXV0aDAiLCJleHAiOjE2NTI3Nzg4MDl9.wNMpIMR3CfEmbizs8wVIXuhkiV5KPdNuQ6LI0ZzgC4Y
```

If the username/password is wrong, returns 401. If correct, it authenticates the user & return a JSON Web Token (JWT). 
As described above, the JWT authorizes the user to access various resources of the service and must be passed in the Authorization header when sending requests. 

#### **Log Out**

As stated before, the server has no concept of session/state. Therefore in stateless authentication, logging out is something that is only done on the client side. The JWT that is returned from logging in will generally be stored in local storage or in cookies. In order to log out, the client will simply delete the JWT from local storage/cookies.

#### **Get Account Information & Delete Account**

To show how the the JWT is used, I implemented GET and DELETE for `/authentications`. For both of these requests, the email address is passed in the URL, and the JWT must be passed in the Authorization Header.

GET `/authentications` returns the user's account information:

```bash
curl -k https://localhost:8443/authentications/[email_address] -H "Authorization: Bearer [json_web_token]"

#Example:
curl -k https://localhost:8443/authentications/abc4@gmail.com -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbF9hZGRyZXNzIjoiYWJjNEBnbWFpbC5jb20iLCJhdXRob3JpemVkX3Njb3BlcyI6WyJHRVRfQVVUSEVOVElDQVRJT04iLCJERUxFVEVfQVVUSEVOVElDQVRJT04iXSwiaXNzIjoiYXV0aDAiLCJleHAiOjE2NTI3Nzg4MDl9.wNMpIMR3CfEmbizs8wVIXuhkiV5KPdNuQ6LI0ZzgC4Y"
```
Returns 401 if JWT is invalid or expired, 404 if the account does not exist (was deleted?), or the user's information if everything is valid.

DELETE `/authentications` deletes the user's account from the database:

```bash
curl -k -X DELETE https://localhost:8443/authentications/[email_address] -H "Authorization: Bearer [json_web_token]"

#Example:
curl -k -X DELETE https://localhost:8443/authentications/abc4@gmail.com -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbF9hZGRyZXNzIjoiYWJjNEBnbWFpbC5jb20iLCJhdXRob3JpemVkX3Njb3BlcyI6WyJHRVRfQVVUSEVOVElDQVRJT04iLCJERUxFVEVfQVVUSEVOVElDQVRJT04iXSwiaXNzIjoiYXV0aDAiLCJleHAiOjE2NTI3Nzk0OTl9.Rg7IBmi2b1WpFP_R0ioVPW0De0oBFS0I-R-y0iVRHvw"
```

### **v2**

Stateful authentication (v2) uses two resources: `/accounts` and `/sessions`.

#### **Sign Up**

To sign up a new user, you send a POST request to `/accounts` with the email address and password as arguments in the body:

```bash
curl -k -v https://localhost:8443/accounts -d emailAddress=[email_address] -d password=[password]

#Example:
curl -k -v https://localhost:8443/accounts -d emailAddress=abc4@gmail.com -d password=abc123456
```

Returns 200 if success or 409 if the email address is already used.

#### **Log In**

To log in a signed-up user, you send a POST request to `/sessions` with the email address and password as arguments in the body:

```bash
curl -k -v https://localhost:8443/auth-tokens -d emailAddress=[email_address] -d password=[password]

#Example:
curl -k -v https://localhost:8443/auth-tokens -d emailAddress=abc4@gmail.com -d password=abc123456
#Returns:
gOAGOCDb_z6eybOdFIrdmY_YRhQ
```

If the username/password is wrong, returns 401. If correct, it authenticates the user, creates a session in the Session table, and returns a session token. Now whenever the user wants to access the service, the session token must be passed in the Authorization Header.

#### **Log Out**
To log out, you send a DELETE request to `/sessions` with the email addres in the URL and the session token in the authorization header:

```bash
curl -k -X DELETE https://localhost:8443/sessions/[email_address] -H "Authorization: Bearer [session_token]"

#Example:
curl -k -X DELETE https://localhost:8443/sessions/abc4@gmail.com -H "Authorization: Bearer gOAGOCDb_z6eybOdFIrdmY_YRhQ"
```

If the session token is valid or expired, deletes the session from the db.

#### **Delete Account**
I implemented this mainly for my use. You can delete an account by sending a DELETE request to `/accounts` with the email address in the URL and the session token in the authorization header:

```bash
curl -k -X DELETE https://localhost:8443/accounts/[email_address] -H "Authorization: Bearer [session_token]"

#Example:
curl -k -X DELETE https://localhost:8443/accounts/abc4@gmail.com -H "Authorization: Bearer Ds7-0XWAqETDqUq9deYfOM1JJH4"
```

If the session token is valid, deletes the account and session from the db. If the session token is expired, deletes only the session from the db.

## Important Dependencies, Libraries, and Tools

#### **Spring Boot**

To implement this project I was debating between Spring Boot & NodeJs. Node Js is great for little projects like this, but I decided to go with Sprint Boot as I have much more experience with Java and Spring Boot has a lot of useful libraries. Spring Boot is also multithreaded which would be important if this project were to scale.

#### **MySQL**

I used MySQL over PostgreSQL mainly because it is lighter weight and I have more experience with it. At scale, PostgreSQL handles concurency better & MySQL can have a faster read. For a stateful auth system, the faster read could be useful to verify session information. PostgresSQL is generally built with extensibility in mind, so if the databases of this project were to get much more complicated then PostGreSQL could be a better option.

#### **Json Web Token**

I already talked about JWT a lot, but JWT is an industry standard for use cases that require an object data stored in a string, and is commonly used for stateless authentication. The library I used to encode/decode the JWT is Java JWT (https://github.com/auth0/java-jwt).

#### **BCrypt / Sprint Security** (https://spring.io/projects/spring-security/)

Spring Security provides several useful security features. It allows us to specify what types of requests can hit the server (I enabled all for the sake of this project, but in production it would be much stricter). I mainly used it as it provides the BCrypt hash function which is a commonly used and very secure hasing algorithm.

#### **Hibernate / Spring Data Java Persistence API** (https://spring.io/projects/spring-data-jpa)
JPA is a collection of specifications for object-relational mapping (ORM). Hibernate implements these specifications and allows us to convert Java objects to database tables. The Spring Data JPA package integrates this into spring boot applications, and makes it very easy and clean to interact with the database layer.

#### **Lombok** (https://projectlombok.org/):
Lombok is Java library that automatically generates useful boilerplate code like getters, setters, constructors, and buiders for objects by just adding annotations.

## Future Work:
If the project were to keep developing here are some of the things I would add next:
- Add email validation
- Get a proper SSL certificate from a Certificate Authority
- Add linking to relevant operations using Spring HATEOAS (https://spring.io/projects/spring-hateoas)