package com.piyush.arraytest.authserver.v2.exceptions;

public class AccountNotFoundException extends RuntimeException {
    public AccountNotFoundException(String emailAddress) {
        super("Wrong email address or password.");
    }
}
