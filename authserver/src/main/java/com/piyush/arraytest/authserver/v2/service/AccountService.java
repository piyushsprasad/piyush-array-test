package com.piyush.arraytest.authserver.v2.service;

import java.util.Optional;


import com.piyush.arraytest.authserver.v2.AccountRepository;
import com.piyush.arraytest.authserver.v2.exceptions.AccountNotFoundException;
import com.piyush.arraytest.authserver.v2.model.Account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    @Autowired
    private AccountRepository accountRepository;

    public void assertAuthorized(String emailAddress, String password) {
        Optional<Account> possibleAccount = accountRepository.findById(emailAddress);

        String hashedPassword = possibleAccount
            .orElseThrow(() -> new AccountNotFoundException(emailAddress))
            .getHashedPassword();
        if (!BCrypt.checkpw(password, hashedPassword)) {
            throw new AccountNotFoundException(emailAddress);
        }
    }
    
}
