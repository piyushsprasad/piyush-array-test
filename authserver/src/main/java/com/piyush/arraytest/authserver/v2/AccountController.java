package com.piyush.arraytest.authserver.v2;

import java.util.Optional;

import com.piyush.arraytest.authserver.v2.model.Account;
import com.piyush.arraytest.authserver.v2.service.SessionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class AccountController {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private SessionService sessionService;    

    // Sign up
    @PostMapping("/accounts")
    Long newAccount(@RequestParam String emailAddress, @RequestParam String password) {
        Optional<Account> possibleAccount = accountRepository.findById(emailAddress);
        if (possibleAccount.isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT,
                    "Email address already used. Please use a different one.");
        }

        Account account = Account.createAccount(emailAddress, password);
        return accountRepository.save(account).getUserId();

    }

    @DeleteMapping("/accounts/{emailAddress}")
    void deleteAccount(
        @PathVariable String emailAddress,
        @RequestHeader(value = "Authorization", required = true) String authorizationHeader) {
        Optional<String> possibleToken = sessionService.getTokenIfUserLoggedIn(emailAddress, authorizationHeader);
        if (possibleToken.isPresent()) {
            sessionRepository.deleteById(possibleToken.get());
            accountRepository.deleteById(emailAddress);
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User not logged in.");
        }
    }

    
}
