package com.piyush.arraytest.authserver.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Required to allow http requests due to spring security dependency. In
 * production this would be much more complicated and secure.
 */
@Configuration
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        // http.requiresChannel(channel -> channel.anyRequest().requiresSecure());
        http.authorizeRequests().antMatchers("auth-tokens", "authentications", "accounts", "sessions").permitAll();

        // Required to send requests not in browser (i.e curl)
        http.csrf().disable();
    }

}