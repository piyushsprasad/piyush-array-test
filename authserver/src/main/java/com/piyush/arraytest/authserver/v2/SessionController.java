package com.piyush.arraytest.authserver.v2;

import java.util.Optional;

import com.piyush.arraytest.authserver.v2.model.Session;
import com.piyush.arraytest.authserver.v2.service.AccountService;
import com.piyush.arraytest.authserver.v2.service.SessionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SessionController {
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private AccountService accountService;
    @Autowired
    private SessionService sessionService;

    // Log in
    @PostMapping("sessions")
    String newSession(@RequestParam String emailAddress, @RequestParam String password) {
        accountService.assertAuthorized(emailAddress, password);

        Session session = Session.createCurrentSession(emailAddress);
        sessionRepository.save(session);
        return session.getToken();
    }

    // Log out
    @DeleteMapping("sessions/{emailAddress}")
    void deleteSession(
            @PathVariable String emailAddress,
            @RequestHeader(value = "Authorization", required = true) String authorizationHeader) {
        Optional<String> possibleToken = sessionService.getTokenIfUserLoggedIn(emailAddress, authorizationHeader);
        if (possibleToken.isPresent()) {
            sessionRepository.deleteById(possibleToken.get());
        }
    }
    
}
