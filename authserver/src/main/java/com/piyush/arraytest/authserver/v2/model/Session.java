package com.piyush.arraytest.authserver.v2.model;

import java.security.SecureRandom;
import java.time.Duration;
import java.time.Instant;
import java.util.Base64;
import java.util.Base64.Encoder;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Session {
    private static final Duration SESSION_EXPIRATION = Duration.ofHours(2);

    @Id
    @Getter
    @Setter
    private String token;

    @Getter
    @Setter
    private String emailAddress;

    @Getter
    @Setter
    private long expirationTimeSeconds;

    public static Session createCurrentSession(String emailAddress) {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
        Encoder encoder = Base64.getUrlEncoder().withoutPadding();

        String token = encoder.encodeToString(bytes);
        long expirationTimeSeconds = Instant.now().plus(SESSION_EXPIRATION).getEpochSecond();
        Session session = Session.builder()
                .token(token)
                .emailAddress(emailAddress)
                .expirationTimeSeconds(expirationTimeSeconds)
                .build();
        return session;
    }
}
