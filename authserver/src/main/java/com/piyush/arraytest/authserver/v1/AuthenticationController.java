package com.piyush.arraytest.authserver.v1;

import java.util.Optional;

import com.piyush.arraytest.authserver.v1.exceptions.AuthenticationNotFoundException;
import com.piyush.arraytest.authserver.v1.model.AuthScope;
import com.piyush.arraytest.authserver.v1.model.Authentication;
import com.piyush.arraytest.authserver.v1.service.JwtAuthorizationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class AuthenticationController {
    @Autowired
    private AuthenticationRepository authenticationRepository;
    @Autowired
    private JwtAuthorizationService authserverJwtService;

    // Get account information
    @GetMapping("/authentications/{emailAddress}")
    Authentication getAuthentication(
            @PathVariable String emailAddress,
            @RequestHeader(value = "Authorization", required = true) String authorizationHeader) {
        authserverJwtService.assertAuthorized(emailAddress, authorizationHeader, AuthScope.GET_AUTHENTICATION);
        return authenticationRepository.findById(emailAddress)
                .orElseThrow(() -> new AuthenticationNotFoundException(emailAddress));
    }

    // Sign up
    @PostMapping("/authentications")
    Long newAuthentication(@RequestParam String emailAddress, @RequestParam String password) {
        Optional<Authentication> optAuth = authenticationRepository.findById(emailAddress);
        if (optAuth.isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT,
                    "Email address already used. Please use a different one.");
        }
        String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());
        Authentication auth = Authentication.fullScopedUser(emailAddress, hashedPassword);

        return authenticationRepository.save(auth).getUserId();
    }

    // Delete account
    @DeleteMapping("/authentications/{emailAddress}")
    void deleteAuthentication(
            @PathVariable String emailAddress,
            @RequestHeader(value = "Authorization", required = true) String authorizationHeader) {
        authserverJwtService.assertAuthorized(emailAddress, authorizationHeader, AuthScope.DELETE_AUTHENTICATION);

        Authentication auth = authenticationRepository.findById(emailAddress)
                .orElseThrow(() -> new AuthenticationNotFoundException(emailAddress));
        authenticationRepository.delete(auth);
    }
}
