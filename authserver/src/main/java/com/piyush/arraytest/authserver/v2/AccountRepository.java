package com.piyush.arraytest.authserver.v2;

import com.piyush.arraytest.authserver.v2.model.Account;

import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, String> {
}
