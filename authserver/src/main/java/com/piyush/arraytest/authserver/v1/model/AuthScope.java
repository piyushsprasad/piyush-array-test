package com.piyush.arraytest.authserver.v1.model;

/**
 * Scopes access to different authentication functions for users.
 */
public enum AuthScope {
    GET_AUTHENTICATION,
    DELETE_AUTHENTICATION,
}
