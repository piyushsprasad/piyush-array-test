package com.piyush.arraytest.authserver.v2.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.security.crypto.bcrypt.BCrypt;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    @Id
    @Getter
    @Setter
    private String emailAddress;

    @Getter
    @Setter
    private String hashedPassword;

    @Getter
    @Setter
    private Long userId;

    public static Account createAccount(String emailAddress, String password) {
        String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());
        Account account = Account.builder()
            .emailAddress(emailAddress)
            .hashedPassword(hashedPassword)
            .build();

        return account;
    }
}