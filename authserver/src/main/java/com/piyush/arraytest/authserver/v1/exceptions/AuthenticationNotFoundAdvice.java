package com.piyush.arraytest.authserver.v1.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class AuthenticationNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(AuthenticationNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String authenticationNotFoundHandler(AuthenticationNotFoundException e) {
        return e.getMessage();
    }

}
