package com.piyush.arraytest.authserver.v1;

import com.piyush.arraytest.authserver.v1.model.Authentication;

import org.springframework.data.repository.CrudRepository;

public interface AuthenticationRepository extends CrudRepository<Authentication, String> {
}
