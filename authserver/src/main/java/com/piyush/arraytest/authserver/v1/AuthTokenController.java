package com.piyush.arraytest.authserver.v1;

import com.piyush.arraytest.authserver.v1.exceptions.AuthenticationNotFoundException;
import com.piyush.arraytest.authserver.v1.model.Authentication;
import com.piyush.arraytest.authserver.v1.service.JwtAuthorizationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class AuthTokenController {
    @Autowired
    private AuthenticationRepository authenticationRepository;
    @Autowired
    private JwtAuthorizationService authserverJwtService;

    // Log in
    @PostMapping("/auth-tokens")
    String getAuthToken(@RequestParam String emailAddress, @RequestParam String password) {
        Authentication auth = authenticationRepository
                .findById(emailAddress)
                .orElseThrow(() -> new AuthenticationNotFoundException(emailAddress));

        if (!BCrypt.checkpw(password, auth.getHashedPassword())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Wrong email or password.");
        }
        return authserverJwtService.createJwt(auth);
    }
}
