package com.piyush.arraytest.authserver.v1.model;

import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Authentication {
    @Id
    @Getter
    @Setter
    private String emailAddress;

    @Getter
    @Setter
    private String hashedPassword;

    @Getter
    @Setter
    private Long userId;

    // Hibernate does not play nice with array of enums, so have to convert to
    // string array here.
    @Getter
    @Setter
    private String[] scopes;

    public static Authentication fullScopedUser(String emailAddress, String hashedPassword) {
        String[] scopes = Arrays.stream(AuthScope.values()).map(AuthScope::name).toArray(String[]::new);

        return Authentication.builder()
                .emailAddress(emailAddress)
                .hashedPassword(hashedPassword)
                .scopes(scopes)
                .build();
    }
}
