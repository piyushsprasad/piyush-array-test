package com.piyush.arraytest.authserver.v1.service;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.piyush.arraytest.authserver.v1.model.AuthScope;
import com.piyush.arraytest.authserver.v1.model.Authentication;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

/**
 * Service to manage JWT operations, including storing and rotating the secret
 * used in the JWT creation/verification.
 * 
 * In production the secret would be stored in a distributed environment such
 * that all the servers could access the same secret.
 * The default scope for spring boot services is Singelton, so this will not be
 * recreated for each request.
 */
@Service
public class JwtAuthorizationService {
    private static final Duration SECRET_REFRESH_DURATION = Duration.ofDays(90);
    private static final Duration TOKEN_DURATION = Duration.ofHours(3);
    private static final String ISSUER = "auth0";

    private static final String PAYLOAD_EMAIL_KEY = "email_address";
    private static final String PAYLOAD_SCOPES_KEY = "authorized_scopes";

    private static final Pattern AUTHORIZATION_HEADER_PATTERN = Pattern.compile("Bearer\\s(.+)");

    private String secret;
    private Instant secretCreationTime;

    public void assertAuthorized(String emailAddress, String authorizationHeader, AuthScope scope) {
        Matcher authHeaderMatcher = AUTHORIZATION_HEADER_PATTERN.matcher(authorizationHeader);
        if (!authHeaderMatcher.matches()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "Authorization Bearer header invalid or not present.");
        }

        String authToken = authHeaderMatcher.group(1);
        Algorithm algorithm = getAlgorithm();
        DecodedJWT decodedJWT;
        try {
            JWTVerifier verifier = JWT.require(algorithm).withIssuer(ISSUER).build();

            // Automatically checks if JWT is expired.
            decodedJWT = verifier.verify(authToken);
        } catch (JWTVerificationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Unable to verify the JWT authorization token",
                    e);
        }

        Claim emailClaim = decodedJWT.getClaim(PAYLOAD_EMAIL_KEY);
        if (emailClaim.isNull() || !emailClaim.asString().equals(emailAddress)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "Email address not authorized for the specified auth-token.");
        }

        Claim scopeClaim = decodedJWT.getClaim(PAYLOAD_SCOPES_KEY);
        if (!hasScope(scope, scopeClaim)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User not authorized for the specified scope.");
        }
    }

    public String createJwt(Authentication authentication) {
        Algorithm algorithm = getAlgorithm();
        Date expiry = Date.from(Instant.now().plus(TOKEN_DURATION));
        String token;
        try {
            token = JWT.create()
                    .withPayload(getPayloadClaims(authentication))
                    .withExpiresAt(expiry)
                    .withIssuer(ISSUER)
                    .sign(algorithm);
        } catch (JWTCreationException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Exception when creating JWT authorization token", e);
        }
        return token;
    }

    private Algorithm getAlgorithm() {
        Instant currentTime = Instant.now();
        if (secret == null || currentTime.isAfter(secretCreationTime.plus(SECRET_REFRESH_DURATION))) {

            // The secret has a minimum length of 256 bits & each UUID is 128 bits. Thus we
            // combine two UUIDs.
            secret = UUID.randomUUID().toString() + UUID.randomUUID().toString();
            secretCreationTime = currentTime;
        }

        return Algorithm.HMAC256(secret);
    }

    private Map<String, Object> getPayloadClaims(Authentication authentication) {
        Map<String, Object> payloadClaims = new HashMap<String, Object>();
        payloadClaims.put(PAYLOAD_EMAIL_KEY, authentication.getEmailAddress());
        payloadClaims.put(PAYLOAD_SCOPES_KEY, authentication.getScopes());

        return payloadClaims;
    }

    private boolean hasScope(AuthScope desiredScope, Claim scopeClaim) {
        if (scopeClaim.isNull()) {
            return false;
        }

        String[] claims = scopeClaim.asArray(String.class);
        for (String claim : claims) {
            if (claim.equals(desiredScope.name())) {
                return true;
            }
        }

        return false;
    }
}
