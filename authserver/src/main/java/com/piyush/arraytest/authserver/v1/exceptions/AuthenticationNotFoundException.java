package com.piyush.arraytest.authserver.v1.exceptions;

public class AuthenticationNotFoundException extends RuntimeException {
    public AuthenticationNotFoundException(String emailAddress) {
        super("Could not find user " + emailAddress);
    }
}
