package com.piyush.arraytest.authserver.v2;

import com.piyush.arraytest.authserver.v2.model.Session;

import org.springframework.data.repository.CrudRepository;

public interface SessionRepository extends CrudRepository<Session, String> {
}
