package com.piyush.arraytest.authserver.v2.service;

import java.time.Instant;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.piyush.arraytest.authserver.v2.SessionRepository;
import com.piyush.arraytest.authserver.v2.model.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


@Service
public class SessionService {
    @Autowired
    private SessionRepository sessionRepository;

    private static final Pattern AUTHORIZATION_HEADER_PATTERN = Pattern.compile("Bearer\\s(.+)");

    public Optional<String> getTokenIfUserLoggedIn(String emailAddress, String authorizationHeader) {
        Matcher authHeaderMatcher = AUTHORIZATION_HEADER_PATTERN.matcher(authorizationHeader);
        if (!authHeaderMatcher.matches()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "Authorization Bearer header invalid or not present.");
        }

        String sessionToken = authHeaderMatcher.group(1);
        Optional<Session> possibleSession = sessionRepository.findById(sessionToken);
        if (possibleSession.isEmpty()) {
            return Optional.empty();
        }

        Session session = possibleSession.get();
        if (!session.getEmailAddress().equals(emailAddress)) {
            // For security, delete current session to make user log in again
            sessionRepository.delete(session);
            return Optional.empty();
        }

        Instant expirationTime = Instant.ofEpochSecond(session.getExpirationTimeSeconds());
        if (Instant.now().isAfter(expirationTime)) {
            // Delete session as it is expired.
            sessionRepository.delete(session);
            return Optional.empty();
        }
        return Optional.of(sessionToken);
    }
}
